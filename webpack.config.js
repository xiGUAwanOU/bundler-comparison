const path = require('path');

module.exports = {
  mode: 'production',
  context: path.resolve(__dirname, 'src'),
  entry: {
    main: [
      './index1.ts',
      './index2.ts',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].webpack.js',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [ 'ts-loader' ],
      },
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ],
  },
};
