function capitalize(str) {
    var foo = 123;
    var bar = 234;
    console.log(foo + bar);
    return str.toUpperCase();
}

function randomFunction(str) {
    console.log('I am doing random stuff: ' + capitalize('hey!') + (" " + str));
}

(function () {
    randomFunction('what is this?');
})();

var dependency2 = {
    message: 'there is a lot of stuff in it',
    anotherMessage: capitalize('hello world!'),
};

(function () {
    console.log(dependency2.message);
    console.log(dependency2.anotherMessage);
})();
