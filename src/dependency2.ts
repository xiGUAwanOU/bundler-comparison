import { capitalize } from "./common-dependency";

export default {
  message: 'there is a lot of stuff in it',
  anotherMessage: capitalize('hello world!'),
}