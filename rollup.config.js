import multiEntry from '@rollup/plugin-multi-entry';
import typescript from '@rollup/plugin-typescript';

export default {
  input: './src/index?.ts',
  output: {
    file: './dist/main.rollup.full.js'
  },
  plugins: [
    multiEntry(),
    typescript(),
  ],
};
