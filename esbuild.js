const esbuild = require('esbuild');
const path = require('path');

esbuild.buildSync({
  stdin: {
    contents: [
      './index1.ts',
      './index2.ts',
    ].map(f => `import '${f}'`).join('\n'),
    resolveDir: path.resolve(__dirname, 'src'),
  },
  outfile: path.resolve(__dirname, 'dist/main.esbuild.js'),
  tsconfig: 'tsconfig.json',
  minify: true,
  bundle: true
});